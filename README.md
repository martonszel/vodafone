# Vodafone

## Getting Started

- git clone (https://gitlab.com/martonszel/vodafone)

## Prerequisites

Step 1 
- Angular-CLI is hosted as a node_module and to install and use it you’ll need NodeJS and it’s package manager named npm / Node Package Manager /

- To install NodeJS, navigate to the Downloads Section of the NodeJS’ website, download and install the Installer, best supported by your OS.

Step 2 
- `npm install -g @angular/cli`

Step 3
- `npm i ` - To install the dependencies 

Step 4 
- `ng serve --open` - The ng serve command should start a development server on your locahost port 4200, so if you go to your browser and enter the following url: http://localhost:4200 - you should see the working app. 

Step 5
- `json-server --watch contact.json` - The ng serve command should start a development server on your locahost port 3000, so if you go to your browser and enter the following url: http://localhost:3000 - you should see the working server.

## Project 


- Home page
![Contacts](img/Contacts.gif)

- Form & Animation
![Forms&Animation](img/forms.gif)

- Mobile
![Mobile](img/responsive.gif)


## Built With
- [Bootstrap](https://getbootstrap.com/) - Bootstrap is an open source toolkit for developing with HTML, CSS, and JS.
- [json-server](https://www.npmjs.com/package/json-server) - Get a full fake REST API with zero coding in less than 30 seconds (seriously).



