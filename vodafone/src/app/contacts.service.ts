import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  url = 'http://localhost:3000/contacts';

  constructor(private http: HttpClient) { }

  getContacts() {
    return this.http.get(this.url);
  }

  getContact(id: number) {
    return this.http.get(this.url + '/' +  id);
  }

  deleteProduct(id) {
    return this.http.delete(this.url + '/' +  id);
  }
}
