import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactsService } from '../contacts.service';

@Component({
  selector: 'app-contactdetails',
  templateUrl: './contactdetails.component.html',
  styleUrls: ['./contactdetails.component.css']
})
export class ContactdetailsComponent implements OnInit {

  id: string;
  contact: any;

  constructor(private route: ActivatedRoute, private contactService: ContactsService, private router: Router ) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id;
    this.contactService.getContact(id).subscribe(contact => this.contact = contact);
  }

  deleteProduct(id) {
    this.contactService.deleteProduct(id);
    alert('Contact deleted');
    this.router.navigate(['/contacts']);
  }



}
