import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactlistComponent } from './contactlist/contactlist.component';
import { RegisterformComponent } from './registerform/registerform.component';
import { ContactdetailsComponent } from './contactdetails/contactdetails.component';


const routes: Routes = [
  { path: '', redirectTo: '/contacts', pathMatch: 'full'},
  { path: 'contacts', component: ContactlistComponent, data: {animation: 'Contacts'} },
  { path: 'contact-details/:id', component: ContactdetailsComponent },
  { path: 'registerform', component: RegisterformComponent, data: {animation: 'Registerform'} },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
