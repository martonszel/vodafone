import { Component, OnInit } from '@angular/core';
import { ContactsService } from '../contacts.service';
import { map, filter, switchMap } from 'rxjs/operators';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';

@Component({
  selector: 'app-contactlist',
  templateUrl: './contactlist.component.html',
  styleUrls: ['./contactlist.component.css'],
  animations: [
    trigger('listStagger', [
      transition('* <=> *', [
        query(
          ':enter',
          [
            style({ opacity: 0, transform: 'translateY(-15px)' }),
            stagger(
              '100ms',
              animate(
                '550ms ease-out',
                style({ opacity: 1, transform: 'translateY(0px)' })
              )
            )
          ],
          { optional: true }
        ),
        query(':leave', animate('50ms', style({ opacity: 0 })), {
          optional: true
        })
      ])
    ])
  ]
})
export class ContactlistComponent implements OnInit {

  contacts$ ;
  itemsLength: number;

  constructor(private contactService: ContactsService) { }

  ngOnInit() {
    this.contacts$ = this.contactService.getContacts().pipe(map((data: any[]) => {
      this.itemsLength = data.length;
      return data;
      }));
    // this.contactService.getContacts().subscribe(data=>console.log(data))
  }



}
